module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: 'tsconfig.json'
  },
  plugins: [
    "@typescript-eslint",
    "prettier"
  ],
  extends: [
    "airbnb-base",
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended",
    "prettier",
  ],
  rules: {
    "prettier/prettier": "warn",
  }
};
