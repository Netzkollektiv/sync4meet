# Find times to meet with others

Schedule events and draw availabilities on a week-view-like panel.
See common availabilities on one view.

This project is work in progress.

# Setup

This project uses [npm](npmjs.com/).

To install, run `npm install`.

To run a live-reloading preview server, run `npm run dev`.

You can compile a static bundle to `/dist` by running `npm run build`.

This project uses, typescript, [preact](https://preactjs.com/) with [htm](https://github.com/developit/htm) as a front-end framework, and [vite](https://vitejs.dev/) for building and serving.
