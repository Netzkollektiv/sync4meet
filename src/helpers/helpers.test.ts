import Availability from "../types/Availability";
import { addAvailability, simplifyTimeslots } from "./helpers";

describe("Helpers", () => {
  describe("simplifyTimeslots", () => {
    it("simplifies touching timeslots", () => {
      const timeslots = [
        {
          start: new Date("2000-11-11T12:00:00"),
          end: new Date("2000-11-11T13:00:00"),
        },
        {
          start: new Date("2000-11-11T13:00:00"),
          end: new Date("2000-11-11T14:30:00"),
        },
        {
          start: new Date("2000-11-11T15:00:00"),
          end: new Date("2000-11-11T16:30:00"),
        },
      ];
      const simplifiedTimeslots = simplifyTimeslots(timeslots);

      expect(simplifiedTimeslots.length).toBe(2);

      // Assert, that the first two dates were unified.
      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() ===
              new Date("2000-11-11T12:00:00").getTime() &&
            slot.end.getTime() === new Date("2000-11-11T14:30:00").getTime()
        )
      ).toBeTruthy();
      // Assert, that the last interval remained the same.
      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() ===
              new Date("2000-11-11T15:00:00").getTime() &&
            slot.end.getTime() === new Date("2000-11-11T16:30:00").getTime()
        )
      ).toBeTruthy();
    });

    it("simplifies overlapping timeslots", () => {
      const timeslots = [
        { start: new Date(20), end: new Date(40) },
        { start: new Date(0), end: new Date(10) },
        { start: new Date(30), end: new Date(50) },
      ];
      const simplifiedTimeslots = simplifyTimeslots(timeslots);
      expect(simplifiedTimeslots.length).toBe(2);
      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() === new Date(0).getTime() &&
            slot.end.getTime() === new Date(10).getTime()
        )
      ).toBeTruthy();

      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() === new Date(20).getTime() &&
            slot.end.getTime() === new Date(50).getTime()
        )
      ).toBeTruthy();
    });

    it("simplifies including timeslots", () => {
      const timeslots = [
        { start: new Date(80), end: new Date(90) },
        { start: new Date(50), end: new Date(60) },
        { start: new Date(40), end: new Date(70) },
        { start: new Date(0), end: new Date(10) },
      ];
      const simplifiedTimeslots = simplifyTimeslots(timeslots);
      expect(simplifiedTimeslots.length).toEqual(3);

      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() === new Date(0).getTime() &&
            slot.end.getTime() === new Date(10).getTime()
        )
      ).toBeTruthy();

      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() === new Date(40).getTime() &&
            slot.end.getTime() === new Date(70).getTime()
        )
      ).toBeTruthy();

      expect(
        simplifiedTimeslots.some(
          (slot) =>
            slot.start.getTime() === new Date(80).getTime() &&
            slot.end.getTime() === new Date(90).getTime()
        )
      ).toBeTruthy();
    });
  });
  describe("addAvailability", () => {
    it("truncates existing availability at the start", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(10), end: new Date(20) },
        },
      ];
      const newAvailability: Availability = {
        status: "unavailable",
        timeframe: { start: new Date(15), end: new Date(30) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);
      expect(results.length).toBe(2);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 10 &&
            availability.timeframe.end.getTime() === 15
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 15 &&
            availability.timeframe.end.getTime() === 30
        )
      ).toBeTruthy();
    });
    it("truncates existing availability at the end", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(20), end: new Date(40) },
        },
      ];
      const newAvailability: Availability = {
        status: "unavailable",
        timeframe: { start: new Date(15), end: new Date(30) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);
      expect(results.length).toBe(2);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 30 &&
            availability.timeframe.end.getTime() === 40
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 15 &&
            availability.timeframe.end.getTime() === 30
        )
      ).toBeTruthy();
    });
    it("swallows contained availability", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(10), end: new Date(20) },
        },
        {
          status: "partlyAvailable",
          timeframe: { start: new Date(0), end: new Date(30) },
        },
      ];
      const newAvailability: Availability = {
        status: "unavailable",
        timeframe: { start: new Date(0), end: new Date(30) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);
      expect(results.length).toBe(1);

      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 30
        )
      ).toBeTruthy();
    });
    it("slices overarching availability", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(0), end: new Date(40) },
        },
      ];
      const newAvailability: Availability = {
        status: "unavailable",
        timeframe: { start: new Date(20), end: new Date(30) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);
      expect(results.length).toBe(3);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 20
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 30 &&
            availability.timeframe.end.getTime() === 40
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 20 &&
            availability.timeframe.end.getTime() === 30
        )
      ).toBeTruthy();
    });
    it("does all of the above", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(0), end: new Date(10) },
        },
        {
          status: "partlyAvailable",
          timeframe: { start: new Date(10), end: new Date(20) },
        },
        {
          status: "unavailable",
          timeframe: { start: new Date(20), end: new Date(40) },
        },
        {
          status: "available",
          timeframe: { start: new Date(50), end: new Date(60) },
        },
        {
          status: "unavailable",
          timeframe: { start: new Date(70), end: new Date(90) },
        },
        {
          status: "available",
          timeframe: { start: new Date(90), end: new Date(100) },
        },
      ];
      const newAvailability: Availability = {
        status: "partlyAvailable",
        timeframe: { start: new Date(30), end: new Date(80) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);

      expect(results.length).toBe(6);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 10
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "partlyAvailable" &&
            availability.timeframe.start.getTime() === 10 &&
            availability.timeframe.end.getTime() === 20
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 20 &&
            availability.timeframe.end.getTime() === 30
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "partlyAvailable" &&
            availability.timeframe.start.getTime() === 30 &&
            availability.timeframe.end.getTime() === 80
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "unavailable" &&
            availability.timeframe.start.getTime() === 80 &&
            availability.timeframe.end.getTime() === 90
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 90 &&
            availability.timeframe.end.getTime() === 100
        )
      ).toBeTruthy();
    });
    it("deals with overlapping old availabilities", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(0), end: new Date(10) },
        },
        {
          status: "partlyAvailable",
          timeframe: { start: new Date(30), end: new Date(40) },
        },
        {
          status: "unavailable",
          timeframe: { start: new Date(30), end: new Date(50) },
        },
        {
          status: "available",
          timeframe: { start: new Date(40), end: new Date(60) },
        },
      ];
      const newAvailability: Availability = {
        status: "partlyAvailable",
        timeframe: { start: new Date(30), end: new Date(50) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);

      expect(results.length).toBe(3);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 10
        )
      );
      expect(
        results.some(
          (availability) =>
            availability.status === "partlyAvailable" &&
            availability.timeframe.start.getTime() === 30 &&
            availability.timeframe.end.getTime() === 50
        )
      ).toBeTruthy();
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 50 &&
            availability.timeframe.end.getTime() === 60
        )
      ).toBeTruthy();
    });
    it("simplifies overlapping availabilities", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(0), end: new Date(30) },
        },
        {
          status: "available",
          timeframe: { start: new Date(20), end: new Date(40) },
        },
      ];
      const newAvailability: Availability = {
        status: "available",
        timeframe: { start: new Date(40), end: new Date(50) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);

      expect(results.length).toBe(1);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 50
        )
      ).toBeTruthy();
    });
    it("does not add newAvailability for status `undefined`", () => {
      const oldAvailabilities: Availability[] = [
        {
          status: "available",
          timeframe: { start: new Date(0), end: new Date(20) },
        },
        {
          status: "available",
          timeframe: { start: new Date(30), end: new Date(40) },
        },
      ];
      const newAvailability: Availability = {
        status: undefined,
        timeframe: { start: new Date(10), end: new Date(50) },
      };
      const results = addAvailability(oldAvailabilities, newAvailability);

      expect(results.length).toBe(1);
      expect(
        results.some(
          (availability) =>
            availability.status === "available" &&
            availability.timeframe.start.getTime() === 0 &&
            availability.timeframe.end.getTime() === 10
        )
      ).toBeTruthy();
    });
  });
});
