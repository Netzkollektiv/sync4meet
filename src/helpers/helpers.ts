import Availability from "../types/Availability";
import Interval from "../types/times/Interval";

export function maxDate(date1: Date, date2: Date): Date {
  return date1.getTime() > date2.getTime() ? date1 : date2;
}

export function sortIntervalsByStart(intervals: Interval[]) {
  return intervals.sort(
    (interval1, interval2) =>
      interval1.start.getTime() - interval2.start.getTime()
  );
}

/**
 * Takes a list of Intervals and unifies overlapping ones.
 * E.g.
 * ```
 * slot1 (0,3) |--|
 * slot4 (2,4)   |--|
 * slot2 (3,6)    |--|
 * slot3 (9,10)        |-|
 * ```
 * would become
 * ```
 * slot1 (0,6) |-----|
 * slot2 (9,10)        |-|
 * ```
 *
 * @param timeslots The timeslots to simplify.
 *   The slots are assumed to be closed at the beginning and open at the end.
 *   The slots do not need to be sorted.
 * @returns A new list of (merged) timeslots.
 */
export function simplifyTimeslots(timeslots: Interval[]): Interval[] {
  if (timeslots.length === 0) {
    return [];
  }

  const sortedByStart = sortIntervalsByStart(timeslots);

  // Traverse `sortedByStart` interval list and unify intervals.
  // The `candidate` to overlap is the current interval for which the end time is not certain yet.
  let candidate = <Interval>sortedByStart[0];
  const resultList: Interval[] = [];

  for (let i = 1; i < sortedByStart.length; i += 1) {
    const currentInterval = <Interval>sortedByStart[i];
    // If the candidate's end overlaps the next intervals start, they can be unified.
    if (candidate.end >= currentInterval.start) {
      candidate = {
        start: candidate.start,
        end: maxDate(currentInterval.end, candidate.end),
      };
    } else {
      // The current candidate does not overlap, so add it to the results.
      resultList.push(candidate);
      // Set the new candidate which might overlap.
      candidate = currentInterval;
    }
  }
  // Add the remaining candidate.
  resultList.push(candidate);

  return resultList;
}

// Credits to: https://keestalkstech.com/2021/10/having-fun-grouping-arrays-into-maps-with-typescript/
export function groupBy<K, V>(array: V[], grouper: (item: V) => K) {
  return array.reduce((store, item) => {
    const key = grouper(item);
    if (!store.has(key)) {
      store.set(key, [item]);
    } else {
      store.get(key)?.push(item);
    }
    return store;
  }, new Map<K, V[]>());
}

/**
 * Uses `simplifyTimeslots` to merge overlapping availabilities of
 * the same status type.
 * @returns A new list of availabilities.
 */
export function simplifyAvailabilities(
  availabilities: Availability[]
): Availability[] {
  // Now merge connecting / overlapping timeslots of the same availability type.
  // Group by availability
  const groupedByStatus = groupBy(
    availabilities,
    (availability) => availability.status || ""
  );

  const simplifiedList: Availability[] = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const status of groupedByStatus.keys()) {
    // Status key `''` means, undefined availability. We leave those out.
    if (status === "") {
      // eslint-disable-next-line no-continue
      continue;
    }
    // Simplify time the availabilities' slots
    const simplifiedTimeslots = simplifyTimeslots(
      groupedByStatus.get(status)?.map((av) => av.timeframe) || []
    );
    // Convert the simplified time slots to availabilities again.
    const simplifiedAvailabilities: Availability[] = simplifiedTimeslots.map(
      (timeframe): Availability => ({
        status,
        timeframe,
      })
    );

    simplifiedList.push(...simplifiedAvailabilities);
  }
  return simplifiedList;
}

/**
 * Merge an availability change into existing availability list.
 * If the @param newAvailability overlaps existing ones,
 * those are truncated or overwritten.
 * Overlapping time slots are merged.
 *
 * @param oldAvailabilities
 * @param newAvailability If the availability has a status of undefined,
 *   this will not be added to the list but only effect overlapping ones.
 * @returns A new list of availabilities.
 */
export function addAvailability(
  oldAvailabilities: Availability[],
  newAvailability: Availability
): Availability[] {
  const sortedByStart = oldAvailabilities.sort(
    (avail1, avail2) =>
      avail1.timeframe.start.getTime() - avail2.timeframe.start.getTime()
  );

  const modifiedAvailabilities: Availability[] = [];

  // Add `newAvailability` to `resultList`, if status is not `undefined`.
  if (newAvailability.status !== undefined) {
    modifiedAvailabilities.push(newAvailability);
  }

  // Traverse and add old availabilities and modify on overlap.
  for (let i = 0; i < sortedByStart.length; i += 1) {
    const oldAvailability = <Availability>sortedByStart[i];
    // New availability drawn like this: `|====|`
    // Old availability drawn like this: `|----|`

    if (
      // Case:
      // |---|
      //       |===|
      oldAvailability.timeframe.end < newAvailability.timeframe.start
    ) {
      modifiedAvailabilities.push(oldAvailability);
    } else if (
      // Case:
      //      |--|
      // |==|
      oldAvailability.timeframe.start >= newAvailability.timeframe.end
    ) {
      // There are now more overlaps, since the list is traversed ordered by start.
      modifiedAvailabilities.push(...sortedByStart.slice(i));
      break;
    } else if (
      // Case:
      // |---|
      //   |===|
      oldAvailability.timeframe.start < newAvailability.timeframe.start &&
      oldAvailability.timeframe.end <= newAvailability.timeframe.end
    ) {
      // Cut off ending
      modifiedAvailabilities.push({
        status: oldAvailability.status,
        timeframe: {
          start: oldAvailability.timeframe.start,
          end: newAvailability.timeframe.start,
        },
      });
    } else if (
      // Case:
      //   |--|
      // |======|
      oldAvailability.timeframe.start >= newAvailability.timeframe.start &&
      oldAvailability.timeframe.end <= newAvailability.timeframe.end
    ) {
      // Discard availability (its enclosed)
    } else if (
      // Case:
      //   |---|
      // |===|
      oldAvailability.timeframe.start >= newAvailability.timeframe.start &&
      oldAvailability.timeframe.end > newAvailability.timeframe.end
    ) {
      // Cut of beginning
      modifiedAvailabilities.push({
        status: oldAvailability.status,
        timeframe: {
          start: newAvailability.timeframe.end,
          end: oldAvailability.timeframe.end,
        },
      });
    } else if (
      // Case:
      // |------|
      //   |==|
      oldAvailability.timeframe.start < newAvailability.timeframe.start &&
      oldAvailability.timeframe.end > newAvailability.timeframe.end
    ) {
      // create two new availabilities before and after new one.
      modifiedAvailabilities.push({
        status: oldAvailability.status,
        timeframe: {
          start: oldAvailability.timeframe.start,
          end: newAvailability.timeframe.start,
        },
      });
      modifiedAvailabilities.push({
        status: oldAvailability.status,
        timeframe: {
          start: newAvailability.timeframe.end,
          end: oldAvailability.timeframe.end,
        },
      });
    }
  }

  return simplifyAvailabilities(modifiedAvailabilities);
}
