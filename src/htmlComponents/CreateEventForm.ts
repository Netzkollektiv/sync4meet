import { html } from "htm/preact";
import { ChangeEvent } from "preact/compat";
import { useState } from "preact/hooks";
import EventProperties from "../types/EventProperties";
import DayOfWeek from "../types/times/DayOfWeek";
import { timeOfDate } from "../utilities/dates";
import { timeIntervals } from "../utilities/sets";
import DayOfMonthSelect from "./DayOfMonthSelect";
import DayOfWeekSelect from "./DayOfWeekSelect";

export default function CreateEventForm(props: {
  onSubmit: (event: EventProperties) => unknown;
}) {
  const [eventProps, setEventProps] = useState({
    name: "",
    pollType: "CALENDAR_DATES",
    selectType: "TIMES_OF_DAY",
    dueDate: undefined,
    availabilityTypes: ["available", "unavailable"],
    defaultTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    timeIntervalMins: 30,
    timeSlots: [],
  } as EventProperties);

  // Depending on select mode (day-of-week / calendar-date),
  //  either of the following is used.
  const [selectedCalDates, setSelectedCalDates] = useState([] as Date[]);
  const [selectedWeekDays, setSelectedWeekDays] = useState([] as DayOfWeek[]);

  // The date part is used for convenience here only.
  const [startTime, setStartTimeRaw] = useState(new Date("1970-01-01 08:00"));
  const [endTime, setEndTimeRaw] = useState(new Date("1970-01-02 17:00"));
  const setStartTime = (time: string) => {
    setStartTimeRaw(new Date(`1970-01-01 ${time}`));
  };
  const setEndTime = (time: string) => {
    if (time === "00:00") {
      setEndTimeRaw(new Date(`1970-01-01 24:00`));
    } else {
      setEndTimeRaw(new Date(`1970-01-01 ${time}`));
    }
  };

  const onFormChange = (e: ChangeEvent<HTMLElement>) => {
    const targetElement = e.target as HTMLInputElement;
    const key = targetElement.name as keyof EventProperties;
    const newValue = targetElement.value;

    setEventProps({ ...eventProps, [key]: newValue });
  };

  const dayOfMonthIntervals = (dates: Date[]) =>
    dates.map((day) => ({
      start: new Date(
        day.getFullYear(),
        day.getMonth(),
        day.getDate(),
        startTime.getHours(),
        startTime.getMinutes()
      ),
      end: new Date(
        day.getFullYear(),
        day.getMonth(),
        day.getDate() +
          // Add another day, if end time is set to 00:00
          (endTime.getHours() === 0 && endTime.getMinutes() === 0 ? 1 : 0),
        endTime.getHours(),
        endTime.getMinutes()
      ),
    }));

  const dayOfWeekIntervals = (newDays: DayOfWeek[]) =>
    newDays.map((day) => ({
      start: {
        day,
        hour: startTime.getHours(),
        minute: startTime.getMinutes(),
      },
      end: { day, hour: endTime.getHours(), minute: endTime.getMinutes() },
    }));

  const onSubmit = (e: SubmitEvent) => {
    e.preventDefault();

    // Post-processing...
    if (eventProps.pollType === "WEEK_DATES") {
      eventProps.timeSlots = dayOfWeekIntervals(selectedWeekDays);
    } else if (eventProps.pollType === "CALENDAR_DATES") {
      eventProps.timeSlots = dayOfMonthIntervals(selectedCalDates);
    }

    props.onSubmit(eventProps);
  };

  const onWholeDaysSelected = () => {
    // Change start and end time to reflect whole day selection.
    setStartTime("00:00");
    setEndTime("24:00");
  };

  const allowedTimes = [
    ...timeIntervals(
      new Date("1970-01-01 00:00:00"),
      new Date("1970-01-02 00:00:01"),
      (eventProps.timeIntervalMins ?? 15) * 60 * 1000
    ),
  ].map(timeOfDate);
  const allowedStartTimeOptions = allowedTimes
    .slice(0, -1)
    .map((time) => html` <option value=${time}>${time}</option>`);
  const allowedEndTimeOptions = allowedTimes
    .slice(1)
    .map(
      (time) =>
        html` <option value=${time}>
          ${time === "00:00" ? "24:00" : time}
        </option>`
    );

  return html`<div>
    <form>
      <h2>Create an Event</h2>
      <input
        placeholder="Event Name"
        type="text"
        name="name"
        value=${eventProps.name}
        onChange=${onFormChange}
      />

      <div style="display: flex">
        <div>Time Zone</div>
        <select
          id="timezone-select"
          name="defaultTimeZone"
          value=${eventProps.defaultTimeZone}
          onchange=${onFormChange}
        >
          ${Intl.supportedValuesOf("timeZone").map(
            (tz) => html`<option value=${tz}>${tz}</option>`
          )}
        </select>
      </div>

      <div
        id="week-or-month-radios"
        onChange=${(e: ChangeEvent<HTMLElement>) => {
          setSelectedCalDates([]);
          setSelectedWeekDays([]);
          onFormChange(e);
        }}
        name="pollType"
        style="display:flex; flex-direction: column;"
      >
        Select days from...
        <div>
          <input
            id="day-of-week-radio"
            type="radio"
            name="pollType"
            value="WEEK_DATES"
          />
          <label for="day-of-week-radio">day of week</label>
        </div>
        <div>
          <input
            id="day-of-month-radio"
            type="radio"
            name="pollType"
            value="CALENDAR_DATES"
            checked
          />
          <label for="day-of-month-radio">day of month</label>
        </div>
      </div>

      ${eventProps.pollType === "WEEK_DATES" &&
      html` <${DayOfWeekSelect} onChange=${setSelectedWeekDays} /> `}
      ${eventProps.pollType === "CALENDAR_DATES" &&
      html` <${DayOfMonthSelect} onChange=${setSelectedCalDates} /> `}

      <!-- -->
      <div
        id="day-or-times-radios"
        onChange=${onFormChange}
        name="selectType"
        style="display:flex; flex-direction: column;"
      >
        Availability for...
        <div>
          <input
            id="whole-day-radio"
            type="radio"
            name="selectType"
            value="WHOLE_DAYS"
            onclick=${onWholeDaysSelected}
          />
          <label for="whole-day-radio">whole days</label>
        </div>
        <div>
          <input
            id="times-radio"
            type="radio"
            name="selectType"
            value="TIMES_OF_DAY"
            checked
          />
          <label for="times-radio">times</label>
        </div>
      </div>
      ${eventProps.selectType === "TIMES_OF_DAY" &&
      html`
        <div>
          <select
            id="time-start"
            value=${timeOfDate(startTime)}
            onchange=${(e: ChangeEvent<HTMLSelectElement>) =>
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              setStartTime((<any>e.target).value)}
          >
            ${allowedStartTimeOptions}
          </select>
          <select
            id="time-end"
            value=${timeOfDate(endTime)}
            onchange=${(e: ChangeEvent) =>
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              setEndTime((<any>e.target).value)}
          >
            ${allowedEndTimeOptions}
          </select>
        </div>
      `}

      <button onClick=${onSubmit}>Create</button>
    </form>
  </div>`;
}
