import { html } from "htm/preact";
import { CSSProperties } from "preact/compat";
import { useState } from "preact/hooks";
import IndexPosition from "../types/IndexPosition";
import PointerDownType from "../types/PointerDownType";
import DayOfWeek, {
  dayForIndex,
  weekDayIndexForDate,
} from "../types/times/DayOfWeek";
import { cartesianProduct, range } from "../utilities/sets";
import SelectPanel, {
  SelectPanelStyles,
  oneOptionMouseSelectFn,
} from "./SelectPanel";

/**
 * Select Panel for selecting days of a month.
 *
 * Has a text input for selecting the month.
 *
 * The `onChange` event passes the selected set
 *  of days in the `Date`-type.
 *
 */
export default function DayOfMonthSelect(props: {
  onChange: (newDays: Date[]) => void;
  styles?: SelectPanelStyles;
}) {
  const [month, setMonth] = useState(new Date());

  const onMonthChange = (e: InputEvent) => {
    const dateInputEl = <HTMLInputElement>e.currentTarget;
    const monthVal = new Date(dateInputEl.value);
    if (!Number.isNaN(monthVal.getTime())) {
      setMonth(monthVal);
    } else {
      // If the input is invalid, use previous value.
      dateInputEl.value = month.toISOString().substring(0, 7);
    }
  };

  const dayFromPos = (pos: IndexPosition) => {
    const dateWithOverflow =
      pos.y * 7 + pos.x + 2 - weekDayIndexForDate(month) - 7;
    return new Date(
      month.getFullYear(),
      month.getMonth(),
      dateWithOverflow
    ).getDate();
  };

  const tileLabels = [
    ...cartesianProduct([...range(0, 7)], [...range(0, 6)]),
  ].map(([x, y]) => String(dayFromPos({ x, y })));

  const handleSelectionChange = (
    selections: Record<string, PointerDownType>
  ) => {
    const dates = Object.keys(selections)
      .map((key) => key.split(","))
      .map(([x, y]) => ({ x: Number(x), y: Number(y) }))
      .map(dayFromPos)
      .map((dayNum) => new Date(month.getFullYear(), month.getMonth(), dayNum));

    props.onChange(dates);
  };

  // Contains 2-char day name <div>s
  const weekDayLabels = [...range(0, 7)]
    .map((i) => <DayOfWeek>dayForIndex(i))
    .map((day) => html`<div>${day.substring(0, 2)}</div>`);

  const gridStyle: CSSProperties = {
    display: "grid",
    gridTemplateColumns: `repeat(7, ${props.styles?.cellWidth || "2em"})`,
    gridTemplateRows: `repeat(1, ${props.styles?.cellHeight || "2em"})`,
    alignItems: "bottom",
    justifyItems: "center",
  };

  return html`<div>
    <input
      type="month"
      onChange=${onMonthChange}
      value=${month.toISOString().substring(0, 7)}
    />
    <div class="week-day-labels" style=${gridStyle}>${weekDayLabels}</div>
    <${SelectPanel}
      dimension=${{ x: 7, y: 6 }}
      tileLabels=${tileLabels}
      stateFromPointerEvent=${oneOptionMouseSelectFn}
      onChange=${handleSelectionChange}
      styles=${props.styles}
    />
  </div>`;
}
