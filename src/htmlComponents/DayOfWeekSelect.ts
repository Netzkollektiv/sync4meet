import { html } from "htm/preact";
import PointerDownType from "../types/PointerDownType";
import DayOfWeek, { dayForIndex } from "../types/times/DayOfWeek";
import { range } from "../utilities/sets";
import SelectPanel, {
  SelectPanelStyles,
  oneOptionMouseSelectFn,
} from "./SelectPanel";

export default function DayOfWeekSelect(props: {
  onChange: (newDays: DayOfWeek[]) => void;
  styles?: SelectPanelStyles;
}) {
  const handleSelectionChange = (
    selections: Record<string, PointerDownType>
  ) => {
    const selectedIndices = Object.keys(selections).map((key) =>
      Number(key.split(",")[0])
    );
    const days = <DayOfWeek[]>selectedIndices.map((i) => dayForIndex(i));
    props.onChange(days);
  };

  // Contains 2-char day name <div>s
  const weekDayNames = [...range(0, 7)]
    .map((i) => <DayOfWeek>dayForIndex(i))
    .map((day) => html`<div>${day.substring(0, 2)}</div>`);

  return html`<div>
    <${SelectPanel}
      dimension=${{ x: 7, y: 1 }}
      stateFromPointerEvent=${oneOptionMouseSelectFn}
      tileLabels=${weekDayNames}
      onChange=${handleSelectionChange}
      styles=${props.styles}
    />
  </div>`;
}
