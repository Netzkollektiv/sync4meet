// We need to use scrollX and scrollY, thus..
/* eslint-disable no-restricted-globals */
import { html } from "htm/preact";
import { CSSProperties } from "preact/compat";
import { useRef, useState } from "preact/hooks";
import IndexPosition from "../types/IndexPosition";
import PointerDownType from "../types/PointerDownType";
import PointerPosition from "../types/PointerPosition";
import Rectangle, { isWithin } from "../types/Rectangle";
import { cartesianProduct, range } from "../utilities/sets";

export function threeOptionMouseSelectFn(
  event: PointerEvent,
  currentSelection: PointerDownType
): PointerDownType | undefined {
  if (
    (event.buttons === 1 && currentSelection === "primary") ||
    (event.buttons === 2 && currentSelection === "secondary") ||
    (event.buttons === 4 && currentSelection === "tertiary")
  ) {
    return undefined;
  }

  if (event.buttons === 1) {
    return "primary";
  }
  if (event.buttons === 2) {
    return "secondary";
  }
  if (event.buttons === 4) {
    return "tertiary";
  }
  return undefined;
}

export function twoOptionMouseSelectFn(
  event: PointerEvent,
  currentSelection: PointerDownType
): PointerDownType | undefined {
  if (
    (event.buttons === 1 && currentSelection === "primary") ||
    (event.buttons === 2 && currentSelection === "secondary")
  ) {
    return undefined;
  }

  if (event.buttons === 1) {
    return "primary";
  }
  if (event.buttons === 2) {
    return "secondary";
  }
  return undefined;
}

export function oneOptionMouseSelectFn(
  event: PointerEvent,
  currentSelection: PointerDownType
): PointerDownType | undefined {
  if (event.buttons === 1 && currentSelection === "primary") {
    return undefined;
  }

  if (event.buttons === 1) {
    return "primary";
  }
  return undefined;
}

export interface SelectPanelStyles {
  primaryColor?: string;
  secondaryColor?: string;
  tertiaryColor?: string;
  cellHeight?: string;
  cellWidth?: string;
}

/** Type to store coordinate in the form of `<x>,<y>`. */
export type StringCoordinate = string;

function Tile(props: {
  position: IndexPosition;
  classes: string[];
  text?: string;
}) {
  const additionalClasses = props.classes
    .flatMap((str) => str.split(" "))
    .join(" ");

  return html`<div
    class="tile tile-x-${props.position.x} tile-y-${props.position
      .y} ${additionalClasses}"
    data-x=${props.position.x}
    data-y=${props.position.y}
  >
    ${props.text || ""}
  </div>`;
}

/**
 * TODO: Description here
 * @param props
 * @returns
 */
export default function SelectPanel(props: {
  dimension: IndexPosition;
  styles?: SelectPanelStyles;
  tileLabels?: string[];
  columnLabels?: string[] | HTMLElement[];
  rowLabels?: string[] | HTMLElement[];
  stateFromPointerEvent: (
    event: PointerEvent,
    currentSelection: PointerDownType | undefined
  ) => PointerDownType;
  onChange: (selected: Record<StringCoordinate, PointerDownType>) => unknown;
}) {
  /** Initial position during a new selection, triggered by `pointerDown` event.
   * If set, indicates that selection is ongoing.
   */
  const [pointerDownPos, setPointerDownPos] = useState(
    undefined as PointerPosition | undefined
  );
  // The current state of selected tiles,
  // change from active selection rectangle not included.
  // Key format: `<x>,<y>`
  const [selectionState, setSelectionState] = useState(
    {} as Record<StringCoordinate, PointerDownType>
  );
  // The top-left and bottom-right index positions of the selected rectangle dimension.
  const [currentSelectionRect, setCurrentSelectionRect] = useState(
    undefined as Rectangle | undefined
  );
  // The pointer type used to start rectangle selection.
  const [currentPointerType, setCurrentPointerType] = useState(
    undefined as PointerDownType | undefined
  );
  // Ref to the HTML element of the select panel.
  const panelRef = useRef<HTMLElement>();

  /** Given a set of tile selections, a rectangle and a `newPointerType` to (over)write
   * in the rectangle area, @returns a new `Record<position, PointerDownType>`.
   */
  const computeRectMerge = (
    oldSelectionState: Record<StringCoordinate, PointerDownType>,
    newSelectionRect: Rectangle,
    newPointerType: PointerDownType | undefined
  ) => {
    const newStates: Record<StringCoordinate, PointerDownType> = {
      ...oldSelectionState,
    };
    // Set value for all rectangle tiles to `currentPointerType`,
    // or delete keys, if `currentPointerType` is undefined
    for (
      let { x } = newSelectionRect.topLeft;
      x <= newSelectionRect.bottomRight.x;
      x += 1
    ) {
      for (
        let { y } = newSelectionRect.topLeft;
        y <= newSelectionRect.bottomRight.y;
        y += 1
      ) {
        if (newPointerType) {
          newStates[`${x},${y}`] = newPointerType;
        } else {
          delete newStates[`${x},${y}`];
        }
      }
    }
    return newStates;
  };

  // # Pointer events & helpers

  // ## Pointer helpers
  /** Computes the tile index position of a pointer position in the select panel.
   * @param pos The absolute position of the Pointer in the window.
   * */
  const indexPosFromPointerPos = (
    pos: PointerPosition
  ): IndexPosition | undefined => {
    if (!panelRef.current) {
      return undefined;
    }

    // First, we try to get the position of the element by checking,
    // if the position is at a panel.
    const clickedEl = document.elementFromPoint(
      pos.x - scrollX,
      pos.y - scrollY
    );
    if (
      panelRef.current.contains(clickedEl) &&
      clickedEl?.getAttribute("data-x") &&
      clickedEl?.getAttribute("data-y")
    ) {
      return {
        x: Number(clickedEl?.getAttribute("data-x")),
        y: Number(clickedEl?.getAttribute("data-y")),
      };
    }

    // If the position is not over a tile, we take this approach:
    // Compute the dimensions of the panel, then height and width
    //  of the tiles, then the index position (assuming the panel
    //  has infinite height/width), then bounding the index positions.
    const topLeftRect = panelRef.current
      .querySelector(".tile-x-0.tile-y-0")
      ?.getBoundingClientRect();
    const bottomRightRect = panelRef.current
      .querySelector(
        `.tile-x-${props.dimension.x - 1}.tile-y-${props.dimension.y - 1}`
      )
      ?.getBoundingClientRect();
    if (!bottomRightRect || !topLeftRect) {
      return undefined;
    }

    const topLeftPoint = {
      x: topLeftRect.x + scrollX,
      y: topLeftRect.y + scrollY,
    };
    const bottomRightPoint = {
      x: bottomRightRect.right + scrollX,
      y: bottomRightRect.bottom + scrollY,
    };

    const width = (bottomRightPoint.x - topLeftPoint.x) / props.dimension.x;
    const height = (bottomRightPoint.y - topLeftPoint.y) / props.dimension.y;
    // Those values may exceed the boundaries of the panel...
    const unbound = {
      x: Math.floor((pos.x - topLeftPoint.x) / width),
      y: Math.floor((pos.y - topLeftPoint.y) / height),
    };
    // ...Therefore, we limit it to the select panel's dimensions.
    const bound = {
      x: Math.min(Math.max(0, unbound.x), props.dimension.x - 1),
      y: Math.min(Math.max(0, unbound.y), props.dimension.y - 1),
    };
    return bound;
  };
  const computeSelectionRect = (
    pointerPos1: PointerPosition,
    pointerPos2: PointerPosition
  ): Rectangle | undefined => {
    const indexPos1 = indexPosFromPointerPos(pointerPos1);
    const indexPos2 = indexPosFromPointerPos(pointerPos2);
    if (!indexPos1 || !indexPos2) {
      return undefined;
    }
    const minX = Math.floor(Math.min(indexPos1.x, indexPos2.x));
    const minY = Math.floor(Math.min(indexPos1.y, indexPos2.y));
    const maxX = Math.floor(Math.max(indexPos1.x, indexPos2.x));
    const maxY = Math.floor(Math.max(indexPos1.y, indexPos2.y));
    return {
      topLeft: { x: minX, y: minY },
      bottomRight: { x: maxX, y: maxY },
    };
  };
  /** Stateful. Resets ongoing selections. */
  const abortSelection = () => {
    setPointerDownPos(undefined);
    setCurrentPointerType(undefined);
    setCurrentSelectionRect(undefined);
  };

  // ## Pointer events
  const onPointerDown = (event: PointerEvent) => {
    const eventPointerPos: PointerPosition = {
      x: event.x + scrollX,
      y: event.y + scrollY,
    };
    const eventIndexPos = indexPosFromPointerPos(eventPointerPos);
    const pointerTypeAtTilePos =
      eventIndexPos && selectionState[`${eventIndexPos.x},${eventIndexPos.y}`];

    setCurrentPointerType(
      props.stateFromPointerEvent(event, pointerTypeAtTilePos)
    );

    setPointerDownPos(eventPointerPos);

    setCurrentSelectionRect(
      computeSelectionRect(eventPointerPos, eventPointerPos)
    );
  };
  const onPointerMove = (event: PointerEvent) => {
    if (!pointerDownPos) {
      // No selection ongoing / Component not mounted.
      return;
    }
    if (event.buttons === 0) {
      abortSelection();
      return;
    }
    const eventPointerPos = { x: event.x + scrollX, y: event.y + scrollY };

    setCurrentSelectionRect(
      computeSelectionRect(pointerDownPos, eventPointerPos)
    );
  };
  const onPointerUp = (event: PointerEvent) => {
    if (!pointerDownPos) return;

    const eventPointerPos = { x: event.x + scrollX, y: event.y + scrollY };
    const selectedRect = computeSelectionRect(pointerDownPos, eventPointerPos);
    if (!selectedRect) return;

    const newState = computeRectMerge(
      selectionState,
      selectedRect,
      currentPointerType
    );
    setSelectionState(newState);
    props.onChange(newState);

    abortSelection();
  };

  // Create HTML tiles for every (x, y) position.
  const tiles = [
    ...cartesianProduct(
      [...range(0, props.dimension.x)],
      [...range(0, props.dimension.y)]
    ),
  ].map(([x, y], i) =>
    Tile({
      position: { x, y },
      text: props?.tileLabels?.at(i),
      classes: [
        String(selectionState[`${x},${y}`] || ""),
        currentSelectionRect && isWithin(currentSelectionRect, { x, y })
          ? "in-selection-rect"
          : "",
      ],
    })
  );
  const cellHeight = props.styles?.cellHeight || "2em";
  const cellWidth = props.styles?.cellWidth || "2em";

  const styles: CSSProperties = {
    display: "grid",
    position: "relative",
    gridTemplateColumns: `repeat(${props.dimension.x}, ${cellWidth})`,
    gridTemplateRows: `repeat(${props.dimension.y}, ${cellHeight})`,
    zIndex: pointerDownPos ? 2 : undefined,
    userSelect: "none",
  };
  const columnLabelStyles: CSSProperties = {
    display: "grid",
    position: "relative",
    gridTemplateColumns: `repeat(${props.dimension.x + 1}, ${cellWidth})`,
  };
  const rowLabelStyles: CSSProperties = {
    display: "grid",
    position: "relative",
    gridTemplateRows: `repeat(${props.dimension.y}, ${cellHeight})`,
    width: cellWidth,
  };
  // Covers the whole screen with a gray background during selection.
  const shroudStyles: CSSProperties = {
    position: "fixed",
    display: pointerDownPos ? "block" : "none",
    zIndex: 1,
    left: 0,
    top: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.1)",
  };

  // TODO: esc to abort selection does not work.
  // It would be nice to track the global pointerUp event too
  //  this way, the selection could abort even if the pointer
  //  is outside of the window.
  //  Also, there is a weird error message about
  //  e.target.getAttribute is not a function when releasing
  //  the pointer outside the window.
  // TODO: cells go transparent when starting select from outside
  return html` <div class="select-panel-wrapper">
    ${props.columnLabels &&
    html`
      <div class="select-panel-column-labels" style=${columnLabelStyles}>
        ${["" /* extra for top left cell */, ...(props.columnLabels || [])].map(
          (label) => html`<div>${label}</div>`
        )}
      </div>
    `}

    <div style="display: flex; flex-flow: row">
      ${props.rowLabels &&
      html`
        <div class="select-panel-row-labels" style=${rowLabelStyles}>
          ${props.rowLabels?.map((label) => html`<div>${label}</div>`)}
        </div>
      `}

      <div
        class="select-panel"
        onContextMenu=${(e: Event) => e.preventDefault()}
        onPointerDown=${onPointerDown}
        onPointerUp=${onPointerUp}
        onMouseMove=${onPointerMove}
        onKeyDown=${(event: KeyboardEvent) =>
          event.key === "Escape" ?? abortSelection()}
      >
        <div class="overlay" style=${shroudStyles}></div>
        <div class="select-grid" style=${styles} ref=${panelRef}>${tiles}</div>
      </div>
    </div>
  </div>`;
}
