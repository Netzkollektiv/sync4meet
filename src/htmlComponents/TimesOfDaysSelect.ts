/* eslint-disable no-restricted-syntax */
import { html } from "htm/preact";
import { simplifyTimeslots } from "../helpers/helpers";
import PointerDownType from "../types/PointerDownType";
import Interval from "../types/times/Interval";
import { timeOfDate } from "../utilities/dates";
import { timeIntervals } from "../utilities/sets";
import SelectPanel, {
  SelectPanelStyles,
  StringCoordinate,
  twoOptionMouseSelectFn,
} from "./SelectPanel";

export default function TimesOfDaysSelect(props: {
  /** Called on selection change. Time slots are joined and sorted by start time. */
  onChange: (times: Record<PointerDownType, Interval[]>) => void;
  days: Date[];
  /** The interval size in minutes. */
  timeInterval: 5 | 10 | 15 | 20 | 30 | 60;
  /** The start and end time of the day for which the user can select. */
  allowedTimes: Interval;
  styles?: SelectPanelStyles;
}) {
  const { start: startTime, end: endTime } = props.allowedTimes;
  const timeslotLengthMs = props.timeInterval * 60_000;
  // Calculate start times from `startTime`.
  const startTimeDay = new Date(
    startTime.getFullYear(),
    startTime.getMonth(),
    startTime.getDate()
  );
  // The ms offset from the start of the `startTime`
  //  and that day's start. E.g. for a start time of 01:00 this would be 60 * 60 * 1000
  const startTimeOffsetMs = startTime.getTime() - startTimeDay.getTime();

  const styles = { cellHeight: "1em", cellWidth: "2em", ...props.styles };

  // Sanitize `props.days`, i.e. remove times of day.
  const days = props.days.map(
    (day) => new Date(day.getFullYear(), day.getMonth(), day.getDate())
  );

  const dayTimeSlots = [...timeIntervals(startTime, endTime, timeslotLengthMs)];

  const handleSelectionChange = (
    selections: Record<StringCoordinate, PointerDownType>
  ) => {
    const availabilities: Record<PointerDownType, Interval[]> = {
      primary: [],
      secondary: [],
      tertiary: [],
    };

    // For primary, secondary, tertiary selections...
    for (const pointerDownType of [
      "primary",
      "secondary",
      "tertiary",
    ] as PointerDownType[]) {
      // Translate selections for the current pointerDownType to timeslots.
      const timeSlots = Object.keys(selections)
        .filter((key) => selections[key] === pointerDownType)
        .map((key) => key.split(","))
        .map(([xStr, yStr]) => {
          const x = Number(xStr);
          const y = Number(yStr);
          const day = <Date>days[x];
          const start = new Date(
            day.getTime() + y * timeslotLengthMs + startTimeOffsetMs
          );
          const end = new Date(start.getTime() + timeslotLengthMs);
          return { start, end } as Interval;
        });
      const simplifiedTimeslots = simplifyTimeslots(timeSlots);
      availabilities[pointerDownType] = simplifiedTimeslots;
    }

    props.onChange(availabilities);
  };

  const dayLabels = props.days.map(
    (date) =>
      html`<div>
        <div>
          ${date.toLocaleDateString(navigator.languages, { weekday: "short" })}
        </div>
        <div class="date-short">
          ${date.toLocaleDateString(navigator.languages, {
            dateStyle: "short",
          })}
        </div>
      </div>`
  );

  const rowTimeLabels = dayTimeSlots.map(timeOfDate);

  // Duplicate each label, for every day.
  const timeLabels = dayTimeSlots
    .flatMap((slot) => new Array(days.length).fill(slot))
    .map(timeOfDate);

  // TODO: If the time windows for the select overlap (e.g. `endTime`
  //  is on 02:00 of the next day and the `startTime` is at 1:00),
  //  selection is not in sync right now.
  // TODO: Some sort of emphasise on full hour grid lines would be nice.
  return html` <div class="times-of-days-select">
    <${SelectPanel}
      dimension=${{ x: days.length, y: dayTimeSlots.length }}
      stateFromPointerEvent=${twoOptionMouseSelectFn}
      tileLabels=${timeLabels}
      rowLabels=${rowTimeLabels}
      columnLabels=${dayLabels}
      onChange=${handleSelectionChange}
      styles=${styles}
    />
    <style>
      .times-of-days-select .tile {
        font-size: 0.6em;
        font-family: monospace;
      }
      .select-panel-row-labels div {
        font-size: 0.6em;
        font-family: monospace;
        width: ${styles.cellWidth};
        align-self: center;
        justify-self: center;
        width: auto;
      }
      .select-panel-column-labels div {
        display: flex;
        align-items: center;
        flex-flow: column;
        font-size: 0.85em;
      }
      .select-panel-column-labels .date-short {
        font-size: 0.6em;
      }
    </style>
  </div>`;
}
