import { html } from "htm/preact";
import { render } from "preact";
import EventProperties from "types/EventProperties";
import CreateEventForm from "./htmlComponents/CreateEventForm";
import TimesOfDaysSelect from "./htmlComponents/TimesOfDaysSelect";

function App() {
  return html` <div>
    <${CreateEventForm}
      onSubmit=${(event: EventProperties) =>
        console.log("event poll created! ", event)}
    />
    <${TimesOfDaysSelect}
      onChange=${console.info}
      days=${[
        new Date("2023-06-13"),
        new Date("2023-06-14"),
        new Date("2023-06-15"),
        new Date("2023-06-16"),
      ]}
      timeInterval=${30}
      allowedTimes=${{
        start: new Date("2023-01-01 09:00:00"),
        end: new Date("2023-01-01 19:00:00"),
      }}
    />
  </div>`;
}

render(html`<${App} />`, document.getElementById("root"));
