import AvailabilityState from "./AvailabilityState";
import Interval from "./times/Interval";

export default interface Availability {
  timeframe: Interval;
  status: AvailabilityState;
}
