type AvailabilityState =
  | undefined
  | "available"
  | "unavailable"
  | "partlyAvailable";

export default AvailabilityState;
