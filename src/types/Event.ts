import EventProperties from "./EventProperties";
import User from "./User";

export default interface Event {
  users: User[];
  properties: EventProperties;
}
