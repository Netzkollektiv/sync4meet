import AvailabilityState from "./AvailabilityState";
import Interval from "./times/Interval";
import Timezone from "./times/Timezone";
import WeekInterval from "./times/WeekInterval";

/** Type to distinguish between a poll where the user can
 * choose calendar dates or days of the week.
 */
export type EventPropertiesPollType = "WEEK_DATES" | "CALENDAR_DATES";

/** Type to distinguish between a poll where the user can choose
 * availabilities for whole days only, or for times of a day.
 */
export type EventPropertiesSelectType = "WHOLE_DAYS" | "TIMES_OF_DAY";

interface WeekTimeProperties {
  /** If users can choose from week or calendar dates. */
  pollType: "WEEK_DATES";
  /** The proposed time intervals. Each should start and end on the same time of day.\
   * For week-day polls, the date part should be ignored in favor of the day
   * of week part.\
   */
  timeSlots: WeekInterval[];
}
interface DateTimeProperties {
  /** If users can choose from week or calendar dates. */
  pollType: "CALENDAR_DATES";
  /** The proposed time intervals. Each should start and end on the same time.\
   * For whole-day select polls, the intervals should start at `00:00:00` and
   * end at `00:00:00` of the following day.
   */
  timeSlots: Interval[];
}

interface BasicEventProperties {
  /** The event name. */
  name: string;
  /** The availability types the users can choose from. */
  availabilityTypes: AvailabilityState[];
  /** The default timezone of the event. */
  defaultTimeZone: Timezone;
  /** The time, the poll closes */
  dueDate?: Date;
  /** If users can choose from week or calendar dates. */
  pollType: EventPropertiesPollType;
  /** If users can choose from whole days or times of the day. */
  selectType: EventPropertiesSelectType;
  /** The interval size between time selections in minutes.\
   * For whole-day select polls, the time slot length should be `undefined`
   * or `24 * 60`.
   */
  timeIntervalMins: number | undefined;
}

type EventProperties = BasicEventProperties &
  (DateTimeProperties | WeekTimeProperties);

export default EventProperties;
