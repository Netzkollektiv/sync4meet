type PointerDownType = "primary" | "secondary" | "tertiary";

export default PointerDownType;
