import Position from "./Position";

type PointerPosition = Position;

export default PointerPosition;
