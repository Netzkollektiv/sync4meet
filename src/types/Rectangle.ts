import Position from "./Position";

export default interface Rectangle {
  topLeft: Position;
  bottomRight: Position;
}

/** @returns true, if pos is within the rectangles bounds or on its border. */
export function isWithin(rect: Rectangle, pos: Position): boolean {
  return (
    rect.topLeft.x <= pos.x &&
    rect.topLeft.y <= pos.y &&
    rect.bottomRight.x >= pos.x &&
    rect.bottomRight.y >= pos.y
  );
}
