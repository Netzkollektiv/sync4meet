import Availability from "./Availability";
import Credentials from "./Credentials";

export default interface User {
  availabilities: Availability[];
  credentials: Credentials;
}
