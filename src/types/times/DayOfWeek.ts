type DayOfWeek =
  | "Monday"
  | "Tuesday"
  | "Wednesday"
  | "Thursday"
  | "Friday"
  | "Saturday"
  | "Sunday";

export const allWeekDays: DayOfWeek[] = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

/** @returns 0 for Monday, 1 for Tuesday, ... */
export const indexForDay: (day: DayOfWeek, locale?: string) => number = (
  day,
  // TODO:
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _locale
) =>
  ({
    Monday: 0,
    Tuesday: 1,
    Wednesday: 2,
    Thursday: 3,
    Friday: 4,
    Saturday: 5,
    Sunday: 6,
  }[day]);

/** Index 0 being Monday, 1 Tuesday, ... */
export const dayForIndex = (dayIndex: number) => allWeekDays[dayIndex % 7];

/** A Monday being 0 */
export const weekDayIndexForDate = (date: Date) => (date.getDay() + 6) % 7;

/** Index 0 being Monday.
 * @returns the day name in the current locale.
 */
export const localDayNameForIndex = (dayIndex: number) =>
  new Date(1970, 1, (dayIndex + 2) % 7).toLocaleDateString([], {
    weekday: "long",
  });

/** Returns a date object that has the given week day.
 *
 * Monday is `1970-01-05`, Tuesday `1970-01-05`, ...
 */
export const dateForWeekDay = (day: DayOfWeek) => {
  switch (day) {
    case "Monday":
      return new Date("1970-01-05");
    case "Tuesday":
      return new Date("1970-01-06");
    case "Wednesday":
      return new Date("1970-01-07");
    case "Thursday":
      return new Date("1970-01-08");
    case "Friday":
      return new Date("1970-01-09");
    case "Saturday":
      return new Date("1970-01-10");
    case "Sunday":
      return new Date("1970-01-11");
    default: {
      const exhaustiveCheck: never = day;
      throw new Error(`This day of week (${exhaustiveCheck}) does not exist.`);
    }
  }
};

export default DayOfWeek;
