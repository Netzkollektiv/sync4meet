export default interface Interval {
  start: Date;
  end: Date;
}
