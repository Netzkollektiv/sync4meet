/**
 * The UTC timezone offset in Minutes (e.g. +60)
 * or the timezone name (e.g. 'Europe/Berlin').
 * This might be a provisional type.
 */
type Timezone = number | string;

export default Timezone;
