export default interface WeekDate {
  day:
    | "Monday"
    | "Tuesday"
    | "Wednesday"
    | "Thursday"
    | "Friday"
    | "Saturday"
    | "Sunday";
  hour: number; // IntRange<0, 24>;
  minute: number; // IntRange<0, 60>;
}
