import WeekDate from "./WeekDate";

export default interface WeekInterval {
  start: WeekDate;
  end: WeekDate;
}
