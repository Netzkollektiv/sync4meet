export const timeOfDate = (time: Date) =>
  `${time.getHours().toString().padStart(2, "0")}:${time
    .getMinutes()
    .toString()
    .padStart(2, "0")}`;

export const placeholder = {};
