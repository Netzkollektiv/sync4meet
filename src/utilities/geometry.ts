import Position from "../types/PointerPosition";

/** Check which rectangles are enclosed */
export function isEnclosed(
  mouseOrigin: Position,
  currentPos: Position,
  tile: DOMRect
) {
  const xMax = Math.max(mouseOrigin.x, currentPos.x);
  const yMax = Math.max(mouseOrigin.y, currentPos.y);
  const xMin = Math.min(mouseOrigin.x, currentPos.x);
  const yMin = Math.min(mouseOrigin.y, currentPos.y);

  // eslint-disable-next-line no-restricted-globals
  const tileX = tile.x + scrollX;
  // eslint-disable-next-line no-restricted-globals
  const tileY = tile.y + scrollY;
  const xTrue = tileX < xMax && xMin < tileX + tile.width;
  const yTrue = tileY < yMax && yMin < tileY + tile.width;
  return xTrue && yTrue;
}

export function tmp() {}
