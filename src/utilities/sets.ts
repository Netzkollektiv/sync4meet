/**
 * Returns an iterator with all integers from start to end, end excluded.
 * @param start
 * @param end
 * @returns
 */
export function* range(
  start: number,
  end: number,
  step = 1
): Generator<number> {
  if (!Number.isInteger(start)) {
    throw new Error("Start value is not an integer.");
  }
  if (!Number.isInteger(end)) {
    throw new Error("End value is not an integer.");
  }

  if (start < end) {
    for (let i = start; i < end; i += step) {
      yield i;
    }
  } else if (start > end) {
    for (let i = start; i > end; i -= step) {
      yield i;
    }
  } else {
    // if start == end, nothing to do
  }
}

/** Returns an iterator from the start date up to the end date,
 * in the millisecond interval, end date excluded.
 * @param intervalSizeMS distance between the yielded dates in ms.
 */
export function* timeIntervals(start: Date, end: Date, intervalSizeMS: number) {
  const rangeIterator = range(start.getTime(), end.getTime(), intervalSizeMS);

  for (
    let currentTime: IteratorResult<number, number> = rangeIterator.next();
    !currentTime?.done;
    currentTime = rangeIterator.next()
  ) {
    yield new Date(currentTime.value);
  }
}

export function* cartesianProduct<T1, T2>(
  set1: Iterable<T1>,
  set2: Iterable<T2>
): Generator<[T1, T2]> {
  // eslint-disable-next-line no-restricted-syntax
  for (const v2 of set2) {
    // eslint-disable-next-line no-restricted-syntax
    for (const v1 of set1) {
      yield [v1, v2];
    }
  }
}
